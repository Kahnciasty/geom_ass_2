from Slab import Slab


class Localizator:
	def __init__(self, points, edges, triangles):
		self.points = list(points)
		self.edges = list(edges)
		self.triangles = triangles
		self.events = self.points[:]
		self.events.sort(key=lambda p: p.x)
		self.slabs = []
		self.prepare_slabs()

	def prepare_slabs(self):
		status = []
		for i in range(len(self.events) - 1):
			self.add_or_remove_from_status(status, self.events[i])
			slab = Slab()
			slab.middle = (self.events[i].x + self.events[i + 1].x) / 2
			slab.start_x = self.events[i].x
			slab.append_to_segments(status)
			slab.sort_segments()
			self.slabs.append(slab)

	def add_or_remove_from_status(self, status, event):
		for edge in event.triangulated_edges:
			if edge in status:
				status.remove(edge)
			else:
				status.append(edge)

	def localize(self, point):
		slab = self.select_slab(point)
		adjacent_segment_1 = None
		adjacent_segment_2 = None
		for i in range(1, len(slab.segments)):
			if slab.segments[i].value_for(point.x) > point.y:
				adjacent_segment_1 = slab.segments[i]
				adjacent_segment_2 = slab.segments[i - 1]
				break

		triangle_with_point = None
		for t in adjacent_segment_1.triangles:
			if t in adjacent_segment_2.triangles:
				triangle_with_point = t
		return triangle_with_point

	def select_slab(self, point):
		for i in range(1, len(self.slabs)):
			if self.slabs[i - 1].start_x <= point.x < self.slabs[i].start_x:
				return self.slabs[i - 1]
		return self.slabs[-1]
