from enum import Enum


class Vertex_type(Enum):
	start = 1
	merge = 2
	split = 3
	end = 4
	regular = 5
