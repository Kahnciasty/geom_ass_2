from Point import Point


class Triangle:
	def __init__(self):
		self.edges = set()
		self.points = []
		self.neighbours = []
		self.visited = False
		self.is_in_path = False

	def __str__(self):
		val = ""
		for point in self.points:
			val += str(point) + " "
		return val

	def append_point(self, p):
		self.points.append(p)

	def append_edges(self, e):
		self.edges.add(e)
		e.triangles.add(self)

	def set_neighbours(self):
		for edge in self.edges:
			for triangle in edge.triangles:
				if triangle is not self:
					self.neighbours.append(triangle)

	def visit_neighbours(self, finish, path):
		self.visited = True

		if self is finish:
			return True

		for triangle in self.neighbours:
			if not triangle.visited:
				self.is_in_path = triangle.visit_neighbours(finish, path)
				if self.is_in_path:
					path.append(self)
					return True

		return False

	def get_center(self):
		x_center = y_center = 0
		for point in self.points:
			x_center += point.x
			y_center += point.y
		return Point(x_center/3, y_center/3)
