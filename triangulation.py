from Chain import Chain
from Edge import Edge
from Triangle import Triangle


class Triangulator:
	def __init__(self, points):
		self.points = []
		self.prepare_points(points)
		self.edges = []
		self.prepare_edges()
		self.events = self.points[:]
		self.events.sort(key=lambda p: (p.y, -p.x), reverse=True)
		self.helper = {}

	def triangulate(self):
		self.triangulation()
		polygons = self.split_polygon_into_triangles()
		for polygon in polygons:
			for point in polygon.points:
				point.triangles.append(polygon)
		return self.points, self.edges, polygons

	def divide_into_chains(self):
		top_vertex = self.events[0]
		bottom_vertex = self.events[-1]
		top_vertex_index = self.points.index(top_vertex)
		bottom_vertex_index = self.points.index(bottom_vertex)

		i = top_vertex_index
		while True:
			self.points[i].chain = Chain.left
			if i == bottom_vertex_index:
				break
			i += 1
			if i == len(self.points):
				i = 0

		i = bottom_vertex_index
		while True:
			self.points[i].chain = Chain.right
			if i == top_vertex_index:
				break
			i += 1
			if i == len(self.points):
				i = 0

	def triangulation(self):
		self.divide_into_chains()
		stack = [self.events[0], self.events[1]]
		i = 2
		while i < (len(self.events) - 1):
			if stack[-1].chain != self.events[i].chain:
				for j in range(len(stack) - 1):
					self.append_new_edge(stack.pop(), self.events[i])
				stack.clear()
				stack.append(self.events[i - 1])
				stack.append(self.events[i])

			else:
				last_popped = stack.pop()
				for j in range(len(stack)):
					top_stack = stack[-1]
					new_edge = Edge(top_stack, self.events[i])
					if new_edge.is_inside_polygon(self.points):
						self.append_new_edge(top_stack, self.events[i])
						last_popped = stack.pop()
					else:
						break

				stack.append(last_popped)
				stack.append(self.events[i])

			i += 1

		stack.pop()
		for i in range(len(stack) - 1):
			self.append_new_edge(stack.pop(), self.events[-1])

	def prepare_points(self, points):
		self.points = points
		for p in self.points:
			p.edge = None

	def prepare_edges(self):
		for i in range(0, len(self.points) - 1):
			edge = Edge(self.points[i], self.points[i + 1])
			self.edges.append(edge)
			self.points[i].triangulated_edges.append(edge)
			self.points[i + 1].triangulated_edges.append(edge)
		edge = Edge(self.points[-1], self.points[0], set_edge_to_point=False)
		self.edges.append(edge)
		self.points[-1].triangulated_edges.append(edge)
		self.points[0].triangulated_edges.append(edge)

		for p in self.points:
			p.add_directed_graph_neighbour()

	def append_new_edge(self, p1, p2):
		edge = Edge(p1, p2, set_edge_to_point=False, set_second_edge_to_point=False)
		self.edges.append(edge)

		p1.add_directed_graph_neighbour(p2)
		p2.add_directed_graph_neighbour(p1)
		p1.triangulated_edges.append(edge)
		p2.triangulated_edges.append(edge)

	def split_polygon_into_triangles(self):
		nodes_in_graph = self.points[:]
		polygons = []
		polygon = Triangle()

		start_node = next_node = nodes_in_graph[0]
		last_node, _ = start_node.find_neighbours(self.points)
		while True:
			polygon.append_point(next_node)
			before_last_node = last_node
			last_node = next_node
			next_node = next_node.get_directed_neighbour(before_last_node)

			if not last_node.directed_neighbours:
				nodes_in_graph.remove(last_node)

			if next_node is start_node:
				polygons.append(polygon)
				if not nodes_in_graph:
					return polygons
				polygon = Triangle()
				start_node = next_node = nodes_in_graph[0]
				last_node, _ = start_node.find_neighbours(self.points)
