import csv
import sys

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.lines import Line2D

from BinarySearchTree import BinarySearchTree
from Cleaner import Cleaner
from Edge import Edge
from Localizator import Localizator
from Point import Point
from Vertex_type import Vertex_type
from triangulation import Triangulator


def load_data():
	points = []
	edges = []
	start_point = None
	finish_point = None
	input_file = sys.argv[1]
	with open(input_file) as csv_file:
		csv_reader = csv.reader(csv_file, delimiter=',')
		try:
			first_row = True
			for row in csv_reader:
				if len(row) == 0:
					continue
				if first_row:
					start_point = Point(float(row[0]), float(row[1]))
					finish_point = Point(float(row[2]), float(row[3]))
					first_row = False
					continue

				p1 = Point(float(row[0]), float(row[1]))
				points.append(p1)

			for i in range(0, len(points) - 1):
				edges.append(Edge(points[i], points[i + 1]))
			edges.append(Edge(points[-1], points[0], set_edge_to_point=False))

			for p in points:
				p.set_vertex_type(points)
				p.add_directed_graph_neighbour()
			events = points[:]
			events.sort(key=lambda p: (p.y, -p.x), reverse=True)

		except (ValueError, IndexError) as e:
			print("Wrong input data format. Abort...")
			exit()
	return edges, points, events, start_point, finish_point


def plot(segments, color):
	for segment in segments:
		x = [segment.start.x, segment.end.x]
		y = [segment.start.y, segment.end.y]
		ax.add_line(Line2D(x, y, marker=".", color=color))
	ax.plot(start_point.x, start_point.y, '*', markersize=18, color=color)
	ax.plot(finish_point.x, finish_point.y, '+', markersize=18, color=color)


def plot_triangle(triangle, color, alpha):
	x = []
	y = []
	for point in triangle.points:
		x.append(point.x)
		y.append(point.y)
	ax.fill(x, y, color=color, alpha=alpha)


def save_plot_to_pdf():
	filename = "result.pdf"
	pdf = PdfPages(filename)
	pdf.savefig(fig)
	pdf.close()
	print("Plot has been saved in", filename)


def set_helper(edge, point):
	helper[edge] = point


def execute_monotone_algorithm():
	for point in events:
		if point.type == Vertex_type.start:
			handle_start_vertex(point)
		elif point.type == Vertex_type.end:
			handle_end_vertex(point)
		elif point.type == Vertex_type.split:
			handle_split_vertex(point)
		elif point.type == Vertex_type.merge:
			handle_merge_vertex(point)
		elif point.type == Vertex_type.regular:
			handle_regular_vertex(point)


def handle_start_vertex(point):
	tree.put(point.edge.get_x_center(), point.edge)
	set_helper(point.edge, point)


def handle_end_vertex(point):
	i = points.index(point) - 1

	previous_vertex_helper = helper[points[i].edge]
	if previous_vertex_helper.type == Vertex_type.merge:
		append_new_edge(previous_vertex_helper, point)
	tree.delete(points[i].edge.get_x_center())


def handle_split_vertex(point):
	closest_left_edge = tree.find_closest_left_edge_node(tree.root, point.x).payload
	append_new_edge(point, helper[closest_left_edge])
	set_helper(closest_left_edge, point)
	tree.put(point.edge.get_x_center(), point.edge)
	set_helper(point.edge, point)


def handle_merge_vertex(point):
	i = points.index(point) - 1
	previous_vertex_helper = find_previous_vertex_edge_helper(point)
	if previous_vertex_helper is not None:
		if previous_vertex_helper.type == Vertex_type.merge:
			append_new_edge(previous_vertex_helper, point)
	tree.delete(points[i].edge.get_x_center())

	closest_edge = tree.find_closest_left_edge_node(tree.root, point.x).payload
	if helper[closest_edge].type == Vertex_type.merge:
		append_new_edge(helper[closest_edge], point)
	set_helper(closest_edge, point)


def handle_regular_vertex(point):
	if polygon_interior_lies_to_the_right(point):
		i = points.index(point) - 1
		previous_vertex_helper = helper[points[i].edge]
		if previous_vertex_helper.type == Vertex_type.merge:
			append_new_edge(previous_vertex_helper, point)
		tree.delete(points[i].edge.get_x_center())
		tree.put(point.edge.get_x_center(), point.edge)
		set_helper(point.edge, point)
	else:
		closest_left_edge = tree.find_closest_left_edge_node(tree.root, point.x).payload
		if helper[closest_left_edge].type == Vertex_type.merge:
			append_new_edge(helper[closest_left_edge], point)
		set_helper(closest_left_edge, point)


def polygon_interior_lies_to_the_right(point):
	index = points.index(point)
	if index == 0:
		previous_point = points[len(points) - 1]
	else:
		previous_point = points[index - 1]
	return previous_point.is_above(point)


def append_new_edge(p1, p2):
	new_point_1 = Point(p1.x, p1.y)
	new_point_2 = Point(p2.x, p2.y)
	edges.append(Edge(new_point_1, new_point_2))

	p1.add_directed_graph_neighbour(p2)
	p2.add_directed_graph_neighbour(p1)
	objects_to_amend_after_triangulation.append(p1)
	objects_to_amend_after_triangulation.append(p2)


def find_previous_vertex_edge_helper(point):
	i = points.index(point) - 1
	try:
		return helper[points[i].edge]
	except KeyError:
		return None


def split_monotone_polygon():
	nodes_in_graph = points[:]
	polygons = []
	polygon = []

	start_node = next_node = nodes_in_graph[0]
	last_node, _ = start_node.find_neighbours(points)
	while True:
		polygon.append(next_node)
		before_last_node = last_node
		last_node = next_node
		next_node = next_node.get_directed_neighbour(before_last_node)

		if not last_node.directed_neighbours:
			nodes_in_graph.remove(last_node)

		if next_node is start_node:
			polygons.append(polygon)
			if not nodes_in_graph:
				return polygons
			polygon = []
			start_node = next_node = nodes_in_graph[0]
			last_node, _ = start_node.find_neighbours(points)


def execute_triangulation_algorithm(polygons):
	triangulated_edges = []
	triangulated_points = []
	triangles = []
	for p in polygons:
		triangulator = Triangulator(p)
		triangulation_result_points, triangulation_result_edges, triangulation_result_triangles = triangulator.triangulate()
		triangles.extend(triangulation_result_triangles)
		triangulated_edges.extend(triangulation_result_edges)
		triangulated_points.extend(triangulation_result_points)
	return triangulated_points, triangulated_edges, triangles


def execute_point_location_algorithm(points, edges, triangles):
	localizator = Localizator(points, edges, triangles)
	t1 = localizator.localize(start_point)
	t2 = localizator.localize(finish_point)
	return t1, t2


def find_path(start_triangle, finish_triangle):
	for t in triangles:
		t.set_neighbours()
	path = []
	start_triangle.visit_neighbours(finish_triangle, path)
	return path


def plot_everything(path, groth_size=0.3):
	plot_triangle(start_triangle, "red", 0.6)
	plot_triangle(finish_triangle, "red", 0.6)
	plot(edges, "black")
	number_of_triangles = len(path)
	for i in range(number_of_triangles):
		plot_triangle(path[i], "red", 0.2)
		if i < number_of_triangles - 1:
			base = path[i].get_center()
			pike = path[i + 1].get_center()
			ax.arrow(pike.x, pike.y, base.x - pike.x, base.y - pike.y, head_width=groth_size, head_length=groth_size,
					 fc='k', ec='k', length_includes_head=True)

	if number_of_triangles > 1:
		ax.arrow(path[0].get_center().x, path[0].get_center().y,
				 finish_triangle.get_center().x - path[0].get_center().x,
				 finish_triangle.get_center().y - path[0].get_center().y, head_width=groth_size, head_length=groth_size,
				 fc='k', ec='k', length_includes_head=True)


if __name__ == '__main__':
	fig, ax = plt.subplots()
	helper = {}
	cleaner = Cleaner()
	tree = BinarySearchTree()
	edges, points, events, start_point, finish_point = load_data()
	objects_to_amend_after_triangulation = []
	execute_monotone_algorithm()
	polygons = split_monotone_polygon()
	points, edges, triangles = execute_triangulation_algorithm(polygons)
	cleaner.merge_redundant_edges_and_points(points, edges, objects_to_amend_after_triangulation)
	edges = cleaner.assign_edges_to_triangles(points, triangles)
	start_triangle, finish_triangle = execute_point_location_algorithm(points, edges, triangles)
	path = find_path(start_triangle, finish_triangle)

	plot_everything(path)
	# plot(edges, "black")
	print("Created " + str(len(triangles)) + " triangles.")
	plt.plot()
	save_plot_to_pdf()
	plt.show()
