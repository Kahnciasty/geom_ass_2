class TreeNode:
	def __init__(self, key, val, left=None, right=None, parent=None):
		self.key = key
		self.payload = val
		self.left_child = left
		self.right_child = right
		self.parent = parent
		self.number_of_descendants = 0

	def has_left_child(self):
		return self.left_child

	def has_right_child(self):
		return self.right_child

	def is_left_child(self):
		return self.parent and self.parent.left_child == self

	def is_right_child(self):
		return self.parent and self.parent.right_child == self

	def is_root(self):
		return not self.parent

	def is_leaf(self):
		return not (self.right_child or self.left_child)

	def has_any_children(self):
		return self.right_child or self.left_child

	def has_both_children(self):
		return self.right_child and self.left_child

	def increase_number_of_descendants(self):
		self.number_of_descendants += 1
		if self.parent is not None:
			self.parent.increase_number_of_descendants()

	def decrease_number_of_descendants(self):
		self.number_of_descendants -= 1
		if self.parent is not None:
			self.parent.decrease_number_of_descendants()

	def replace_node_data(self, key, value, lc, rc):
		self.key = key
		self.payload = value
		self.left_child = lc
		self.right_child = rc
		if self.has_left_child():
			self.left_child.parent = self
		if self.has_right_child():
			self.right_child.parent = self


	def find_successor(self):
		succ = None
		if self.has_right_child():
			succ = self.right_child.find_min()
		else:
			if self.parent:
				if self.is_left_child():
					succ = self.parent
				else:
					self.parent.right_child = None
					succ = self.parent.find_successor()
					self.parent.right_child = self
		return succ

	def find_min(self):
		while self.has_left_child():
			self = self.left_child
		return self

	def find_max(self):
		while self.has_right_child():
			self = self.right_child
		return self

	def splice_out(self):
		if self.is_leaf():
			if self.is_left_child():
				self.parent.left_child = None
			else:
				self.parent.right_child = None
		elif self.has_any_children():
			if self.has_left_child():
				if self.is_left_child():
					self.parent.left_child = self.left_child
				else:
					self.parent.right_child = self.left_child
				self.left_child.parent = self.parent
			else:
				if self.is_left_child():
					self.parent.left_child = self.right_child
				else:
					self.parent.right_child = self.right_child
				self.right_child.parent = self.parent
