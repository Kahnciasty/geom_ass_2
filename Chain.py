from enum import Enum


class Chain(Enum):
	left = 1
	right = 2
