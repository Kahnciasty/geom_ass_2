class Slab:
	def __init__(self):
		self.start_x = 0
		self.middle = 0
		self.segments = []

	def append_to_segments(self, status):
		self.segments.extend(status)

	def sort_segments(self):
		self.segments.sort(key=lambda s: s.value_for(self.middle))

