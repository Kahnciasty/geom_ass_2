import numpy as np

from Chain import Chain


class Edge:
	def __init__(self, p1, p2, set_edge_to_point=True, set_second_edge_to_point=True):
		if p1.y == p2.y:
			self.start = max(p1, p2, key=lambda p: p.x)
			self.end = min(p1, p2, key=lambda p: p.x)
		else:
			self.start = min(p1, p2, key=lambda p: p.y)
			self.end = max(p1, p2, key=lambda p: p.y)

		if set_edge_to_point:
			p2.edge = self
		if set_second_edge_to_point:
			p1.edge = self
		self.a, self.b = self.calculate_segments_a_and_b()
		self.angle = self.calculate_angle()
		self.triangles = set()

	def calculate_segments_a_and_b(self):
		if self.start.x == self.end.x:
			x_zero = 0.0001
		else:
			x_zero = self.start.x - self.end.x

		a = (self.start.y - self.end.y) / x_zero
		if a == 0:
			a = 0.0001
		b = self.start.y - a * self.start.x
		return a, b

	def calculate_angle(self):
		angle = np.math.degrees(np.math.atan2(self.end.y - self.start.y, self.end.x - self.start.x))
		if angle < 0:
			angle += 360
		return angle

	def value_for(self, x):
		return self.a * x + self.b

	def get_x_center(self):
		return (self.start.x + self.end.x) / 2

	def __str__(self):
		return str(str(self.start.x) + ", " + str(self.start.y) + ", " + str(self.end.x) + ", " + str(
			self.end.y))

	def is_inside_polygon(self, points):
		start_index = points.index(self.start)
		end_index = points.index(self.end)
		if self.start.chain == Chain.left:
			points_between_endpoints = points[end_index + 1: start_index]
		else:
			points_between_endpoints = points[start_index + 1: end_index]

		return self.all_points_on_correct_side_of_edge(points_between_endpoints)

	def all_points_on_correct_side_of_edge(self, points):
		for point in points:
			if not point.is_on_correct_side_of_edge(self):
				return False
		return True
