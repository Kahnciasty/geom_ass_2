from TreeNode import TreeNode


class BinarySearchTree:
	def __init__(self):
		self.root = None
		self.size = 0

	def length(self):
		return self.size

	def __len__(self):
		return self.size

	def put(self, key, val):
		if self.root:
			self._put(key, val, self.root)
		else:
			self.root = TreeNode(key, val)
		self.size = self.size + 1

	def _put(self, key, val, current_node):
		if key < current_node.key:
			if current_node.has_left_child():
				self._put(key, val, current_node.left_child)
			else:
				current_node.left_child = TreeNode(key, val, parent=current_node)
				current_node.increase_number_of_descendants()
		else:
			if current_node.has_right_child():
				self._put(key, val, current_node.right_child)
			else:
				current_node.right_child = TreeNode(key, val, parent=current_node)
				current_node.increase_number_of_descendants()

	def __setitem__(self, k, v):
		self.put(k, v)

	def get(self, key):
		if self.root:
			res = self._get(key, self.root)
			if res:
				return res.payload
			else:
				return None
		else:
			return None

	def _get(self, key, current_node):
		if not current_node:
			return None
		elif current_node.key == key:
			return current_node
		elif key < current_node.key:
			return self._get(key, current_node.left_child)
		else:
			return self._get(key, current_node.right_child)

	def __getitem__(self, key):
		return self.get(key)

	def __contains__(self, key):
		if self._get(key, self.root):
			return True
		else:
			return False

	def delete(self, key):
		if self.size > 1:
			node_to_remove = self._get(key, self.root)
			if node_to_remove:
				if node_to_remove.parent is not None:
					node_to_remove.decrease_number_of_descendants()
				self.remove(node_to_remove)
				self.size = self.size - 1
			else:
				print('Key not found')
				pass
		elif self.size == 1 and self.root.key == key:
			self.root = None
			self.size = self.size - 1
		else:
			print('Key not found')
			pass

	def __delitem__(self, key):
		self.delete(key)

	def find_min(self):
		current = self.root
		if current is None:
			return None
		while current.has_left_child():
			current = current.left_child
		return current

	def find_max(self):
		current = self.root
		if current is None:
			return None
		while current.has_right_child():
			current = current.right_child
		return current

	def find_closest_left_edge_node(self, node, x):
		max_node = self.find_max()
		if max_node is not None and max_node.key < x:
			return max_node

		if node.has_left_child():
			return self.find_closest_left_edge_node(node.left_child, x)

		if node.is_root():
			return node

		succ = node.find_successor()
		if succ is not None and x < succ.key:
			return node

		if node.has_right_child():
			return self.find_closest_left_edge_node(node)

	def remove(self, current_node):
		if current_node.is_leaf():
			if current_node == current_node.parent.left_child:
				current_node.parent.left_child = None
			else:
				current_node.parent.right_child = None
		elif current_node.has_both_children():
			succ = current_node.find_successor()
			succ.splice_out()
			current_node.key = succ.key
			current_node.payload = succ.payload

		else:
			if current_node.has_left_child():
				if current_node.is_left_child():
					current_node.left_child.parent = current_node.parent
					current_node.parent.left_child = current_node.left_child
				elif current_node.is_right_child():
					current_node.left_child.parent = current_node.parent
					current_node.parent.right_child = current_node.left_child
				else:
					current_node.replace_node_data(current_node.left_child.key,
												   current_node.left_child.payload,
												   current_node.left_child.left_child,
												   current_node.left_child.right_child)
			else:
				if current_node.is_left_child():
					current_node.right_child.parent = current_node.parent
					current_node.parent.left_child = current_node.right_child
				elif current_node.is_right_child():
					current_node.right_child.parent = current_node.parent
					current_node.parent.right_child = current_node.right_child
				else:
					current_node.replace_node_data(current_node.right_child.key,
												   current_node.right_child.payload,
												   current_node.right_child.left_child,
												   current_node.right_child.right_child)
