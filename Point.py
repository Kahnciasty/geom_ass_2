from cmath import sqrt

import numpy as np

from Chain import Chain
from Edge import Edge
from Vertex_type import Vertex_type


class Point:
	def __init__(self, x=0, y=0):
		self.x = x
		self.y = y
		self.edge = None
		self.type = None
		self.directed_neighbours = []
		self.triangulated_edges = []
		self.triangles = []

	def set_vertex_type(self, points):
		n1, n2 = self.find_neighbours(points)
		self.set_interior_angle(n1, n2)

		if self.is_above(n1) and self.is_above(n2):
			if self.interior_angle < 180:
				self.type = Vertex_type.start
				return
			else:
				self.type = Vertex_type.split
				return

		if self.is_below(n1) and self.is_below(n2):
			if self.interior_angle < 180:
				self.type = Vertex_type.end
				return
			else:
				self.type = Vertex_type.merge
				return

		if (self.is_below(n1) and self.is_above(n2)) or (self.is_above(n1) and self.is_below(n2)):
			self.type = Vertex_type.regular
			return

	def find_neighbours(self, points):
		i = points.index(self)
		n1 = points[i - 1]
		if len(points) == i + 1:
			self.n1 = n1
			self.n2 = points[0]
			return n1, points[0]

		self.n1 = n1
		self.n2 = points[i + 1]
		return n1, points[i + 1]

	def is_below(self, point):
		return self.y < point.y or (self.y == point.y and self.x > point.x)

	def is_above(self, point):
		return self.y > point.y or (self.y == point.y and self.x < point.x)

	def set_interior_angle(self, n1, n2):
		a = n1.distance_to(self)
		b = self.distance_to(n2)
		c = n1.distance_to(n2)

		self.interior_angle = np.math.degrees(np.arccos((pow(a, 2) + pow(b, 2) - pow(c, 2)) / (2 * a * b)))

		x1, y1 = n1.x - self.x, n1.y - self.y
		x2, y2 = n2.x - self.x, n2.y - self.y

		if self.reverse_inner_and_outer_angles(x1, y1, x2, y2):
			self.interior_angle = 360 - self.interior_angle

	def calculate_angle(self, n1, n2):
		a = n1.distance_to(self)
		b = self.distance_to(n2)
		c = n1.distance_to(n2)

		angle = np.math.degrees(np.arccos((pow(a, 2) + pow(b, 2) - pow(c, 2)) / (2 * a * b)))

		x1, y1 = n1.x - self.x, n1.y - self.y
		x2, y2 = n2.x - self.x, n2.y - self.y

		if self.reverse_inner_and_outer_angles(x1, y1, x2, y2):
			angle = 360 - angle
		if angle == 0:
			angle = 360
		return angle

	def distance_to(self, point):
		return sqrt(pow(self.x - point.x, 2) + pow(self.y - point.y, 2))

	def reverse_inner_and_outer_angles(self, x1, y1, x2, y2):
		return x1 * y2 > x2 * y1

	def is_on_correct_side_of_edge(self, edge):
		if edge.start.chain == Chain.right and edge.end.chain == Chain.right:
			return not self.is_on_left_side_of(edge)
		elif edge.start.chain == Chain.left and edge.end.chain == Chain.left:
			return self.is_on_left_side_of(edge)
		elif edge.start.chain == Chain.left and edge.end.chain == Chain.right:
			return self.is_on_left_side_of(edge)
		elif edge.start.chain == Chain.right and edge.end.chain == Chain.left:
			return not self.is_on_left_side_of(edge)

	def is_on_left_side_of(self, edge):
		virtual_edge = Edge(edge.start, self)
		return virtual_edge.angle > edge.angle

	def add_directed_graph_neighbour(self, neighbour=None):
		if neighbour is None:
			if self.edge.start is self:
				neighbour = self.edge.end
			else:
				neighbour = self.edge.start

		self.directed_neighbours.append(neighbour)

	def visit_directed_neighbour(self, start_node, points, polygon_points):
		previous_point, _ = self.find_neighbours(points)
		next_node = self.select_left_directed_neighbour(previous_point.edge)
		polygon_points.append(next_node)
		if next_node == start_node:
			pass
		next_node.visit_directed_neighbour(start_node, points, polygon_points)

	def get_directed_neighbour(self, previous_point):
		left_neighbour = self.select_left_directed_neighbour(previous_point)
		self.directed_neighbours.remove(left_neighbour)
		return left_neighbour

	def select_left_directed_neighbour(self, previous_neighbour):
		local_neighbours = self.directed_neighbours[:]
		tab = []
		for n in local_neighbours:
			angle = self.calculate_angle(previous_neighbour, n)
			tab.append((angle, n))

		tab.sort(key=lambda t: t[0])
		return tab[0][1]

	def __str__(self):
		return str(str(self.x) + ", " + str(self.y))
