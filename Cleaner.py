from Edge import Edge


class Cleaner:
	def merge_redundant_edges_and_points(self, points, edges, objects_to_amend_after_triangulation):
		# Remove points that are in multiple figures after splitting monotone polygons
		for obj in objects_to_amend_after_triangulation:
			for i in range(len(points)):
				if points[i].x == obj.x and points[i].y == obj.y:
					points.pop(i)
					break

		# Remove repeated edges after merging different monotone polygons back together
		for point in objects_to_amend_after_triangulation:
			indices_to_remove = []
			for i in range(len(point.triangulated_edges)):
				for j in range(i + 1, len(point.triangulated_edges)):
					if point.triangulated_edges[i].start.x == point.triangulated_edges[j].start.x and \
									point.triangulated_edges[i].start.y == point.triangulated_edges[j].start.y and \
									point.triangulated_edges[i].end.x == point.triangulated_edges[j].end.x and \
									point.triangulated_edges[i].end.y == point.triangulated_edges[j].end.y:
						indices_to_remove.append(j)
			for index in reversed(indices_to_remove):
				point.triangulated_edges.pop(index)

		for i in range(0, len(objects_to_amend_after_triangulation), 2):
			virtual_edge = Edge(objects_to_amend_after_triangulation[i], objects_to_amend_after_triangulation[i + 1])
			for edge in edges:
				if edge.start.x == virtual_edge.start.x and edge.start.y == virtual_edge.start.y and edge.end.x == virtual_edge.end.x and edge.end.x == virtual_edge.end.x:
					edges.remove(edge)
					break

	def assign_edges_to_triangles(self, points, triangles):
		for point in points:
			for triangulated_edge in point.triangulated_edges:
				for triangle in point.triangles:
					if triangulated_edge.start in triangle.points and triangulated_edge.end in triangle.points:
						triangle.append_edges(triangulated_edge)

		siema = set()
		for t in triangles:
			for edge in t.edges:
				siema.add(edge)
		return list(siema)
